<?php

namespace Duna\Security\Components;

interface IUserFormFactory
{

	/** @return UserForm\Component */
	function create($parent, $name, $entity);
}
