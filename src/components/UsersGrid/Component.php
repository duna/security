<?php

namespace Duna\Security\Components\UsersGrid;

use App\Components\Flash\Flash;
use Duna\Security\Entity\User;
use Kdyby\Doctrine\EntityManager;
use Nette;

/**
 * Class Component
 *
 * @package Duna\Security\Components\UsersGrid
 */
class Component extends Nette\Application\UI\Control
{

    /** @var EntityManager */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function render()
    {
        $users = $this->getData();

        $template = $this->template;
        $template->users = $users;
        $template->render(__DIR__ . '/default.latte');
    }

    public function getData()
    {
        return $this->em->getRepository(User::class)->createQueryBuilder('user')
            ->join('user.roles', 'role')->addSelect('role')
            ->getQuery()
            ->getScalarResult();
    }

    public function handleDelete($id)
    {
        $partialUser = $this->em->getPartialReference(User::class, $id);
        $this->em->remove($partialUser);
        $this->em->flush($partialUser);

        $this->presenter->flashMessage('Uživatel úspěšně odstraněn', Flash::SUCCESS);
        $this->redirect('this');
    }

    public function handleLoadData()
    {
        $this->getPresenter()->sendJson(['data' => $this->getData()]);
    }

}
