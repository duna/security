<?php

namespace Duna\Security\Components\UserForm;

use App\Components\Flash\Flash;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Duna\Forms\ControlForm;
use Duna\Forms\Form;
use Duna\Security\Authorizator;
use Duna\Security\Entity\Role;
use Duna\Security\Entity\User;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Security\Components\UserForm
 */
class Component extends ControlForm
{
    private $cache;

    public function __construct($parent, $name, \Doctrine\ORM\EntityManagerInterface $em, $entity, IStorage $storage)
    {
        parent::__construct($parent, $name, $em, $entity);
        $this->cache = new Cache($storage, Authorizator::CACHE_NAMESPACE);
    }

    public function formError(Form $form)
    {
        // TODO: Implement formError() method.
    }

    public function formSuccess(Form $form, $values)
    {
        if ($this->entity === null)
            $this->entity = new User;

        $this->cache->clean([
            Cache::TAGS => [Authorizator::CACHE_NAMESPACE . '/users'],
        ]);
        $this->entity->name = $values->name;
        $this->entity->surname = $values->surname;
        $this->entity->setEmail($values->email);
        $this->entity->setPassword($values->password);
        $this->entity->clearRoles();
        $this->entity->addRole($this->em->getPartialReference(Role::class, $values->role));
        try {
            $this->em->persist($this->entity);
            $this->em->flush($this->entity);
        } catch (UniqueConstraintViolationException $e) {
            $form->addError('Uživatel s daným emailem již existuje');
            $this->getPresenter()->flashMessage('Uživatel s daným emailem již existuje', Flash::DANGER);
        }
    }

    public function render()
    {
        $template = $this->template;
        $template->edit = $this->entity !== null;
        $template->messages = $this->messages;
        $template->render(__DIR__ . '/default.latte');
    }

    public function setDefaults(Form $form)
    {
        if ($this->entity !== null) {
            $e = $this->entity;
            $form->setDefaults([
                'name'    => $e->name,
                'surname' => $e->surname,
                'email'   => $e->email,
                'role'    => $e->roles ? $e->roles[0]->getId() : '',
            ]);
        }
    }

    protected function createComponentUserForm($name)
    {
        $form = $this->createForm($name);

        $form->addProtection();
        $form->addText('name', 'Jméno')
            ->setRequired('Prosím zadejte jméno');

        $form->addText('surname', 'Přijmení')
            ->setRequired('Prosím zadejte přijmení');

        $form->addText('email', 'Email')
            ->setRequired('Prosím zadejte email')
            ->addRule(Form::EMAIL, 'Zadejte email ve správném formátu');

        $e = $form->addPassword('password', 'Heslo');
        if ($this->entity === null)
            $e->setRequired('Prosím zadejte heslo');

        $form->addSelect('role', 'Role', $this->em->getRepository(Role::class)->findPairs('name'));

        $this->setDefaults($form);
        $form->addSubmit('send', 'Odeslat');
        return $form;
    }
}
