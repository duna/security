<?php

namespace Duna\Security\Components;

interface IUsersGrid
{

	/** @return UsersGrid\Component */
	function create();
}
