<?php

namespace Duna\Security\DI;

use App;
use Duna\Console\IEntityConsoleProvider;
use Duna\DI\Extensions\CompilerExtension;
use Duna\Plugin\Manager\Entity\Plugin;
use Duna\Plugin\Manager\IPlugin;
use Duna\Security\Cli\Commands\InstallCommand;
use Kdyby\Doctrine\DI\IEntityProvider;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Security\DI
 */
class Extension extends CompilerExtension implements IPlugin, IEntityProvider, IEntityConsoleProvider
{
    private $commands = [
        InstallCommand::class,
    ];

    public function beforeCompile()
    {
        $builder = $this->getContainerBuilder();
        $this->addPresenterMapping([
            "Security" => 'Duna\\Security\\*Module\\Presenters\\*Presenter',
        ]);
    }

    function getEntityMappings()
    {
        return self::entityMappings();
    }

    public function loadConfiguration()
    {
        $this->parseConfig(__DIR__ . '/config.neon');
        $this->addCommands($this->commands);
    }

    /**
     * @inheritDoc
     */
    public static function getPluginInfo()
    {
        $entity = new Plugin();
        $entity->name = 'Security';
        $entity->description = 'Plugin security';

        return [
            'plugin' => $entity,
        ];
    }


    public static function entityMappings()
    {
        return ["Duna\\Security\\Entity" => __DIR__ . '/../Entity'];
    }
}
