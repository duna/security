<?php

namespace Duna\Security\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="plugin_resource", uniqueConstraints={@ORM\UniqueConstraint(name="resource", columns={"resource"})})
 * @ORM\Entity
 *
 * @method setResource(string $resource)
 * @method setName(string $name)
 */
class Resource
{

    use \Kdyby\Doctrine\Entities\MagicAccessors;

    const CACHE_NAMESPACE = 'System.Resources';
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $resource;
    /**
     * @var integer
     *
     * @ORM\Column(name="resource_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    public function getId()
    {
        return $this->id;
    }

}
