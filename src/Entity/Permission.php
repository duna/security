<?php

namespace Duna\Security\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="plugin_permission", indexes={@ORM\Index(name="role_id", columns={"role_id"}), @ORM\Index(name="resource_id", columns={"resource_id"})})
 * @ORM\Entity
 */
class Permission
{

    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var boolean
     *
     * @ORM\Column(name="c", type="boolean", nullable=false, options={"comment":"create"})
     */
    protected $create = true;
    /**
     * @var boolean
     *
     * @ORM\Column(name="r", type="boolean", nullable=false, options={"comment":"read"})
     */
    protected $read = true;
    /**
     * @var boolean
     *
     * @ORM\Column(name="u", type="boolean", nullable=false, options={"comment":"write"})
     */
    protected $update = true;
    /**
     * @var boolean
     *
     * @ORM\Column(name="d", type="boolean", nullable=false, options={"comment":"remove"})
     */
    protected $delete = true;
    /**
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="role_id", onDelete="cascade")
     */
    protected $role;
    /**
     * @var Resource
     *
     * @ORM\ManyToOne(targetEntity="Resource")
     * @ORM\JoinColumn(name="resource_id", referencedColumnName="resource_id", nullable=false, onDelete="cascade")
     */
    protected $resource;
    /**
     * @var integer
     *
     * @ORM\Column(name="permission_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    public function getId()
    {
        return $this->id;
    }

    public function setCreate($create = true)
    {
        $this->create = (boolean) $create;
        return $this;
    }

    public function setRead($read = true)
    {
        $this->read = (boolean) $read;
        return $this;
    }

    public function setUpdate($update = true)
    {
        $this->update = (boolean) $update;
        return $this;
    }

    public function setDelete($delete = true)
    {
        $this->delete = (boolean) $delete;
        return $this;
    }

}
