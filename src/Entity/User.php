<?php

namespace Duna\Security\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="plugin_user", uniqueConstraints={@ORM\UniqueConstraint(name="email", columns={"email"})})
 * @ORM\Entity
 */
class User
{

    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $name;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $surname;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $email;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $password;
    /**
     * @ORM\ManyToMany(targetEntity="Role", cascade={"persist"})
     * @ORM\JoinTable(name="plugin_user_role",
     *        joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="user_id", onDelete="cascade")},
     *        inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="role_id", onDelete="cascade")}
     * )
     * @var Role[]|\Doctrine\Common\Collections\ArrayCollection
     */
    protected $roles;
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime", nullable=false)
     */
    private $createAt;

    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection;
        $this->createAt = new \DateTime;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreateAt()
    {
        return $this->createAt;
    }

    public function setEmail($email)
    {
        $this->email = \Nette\Utils\Strings::lower($email);
        return $this;
    }

    public function setPassword($password)
    {
        $this->password = \Nette\Security\Passwords::hash($password);
        return $this;
    }

    public function clearRoles()
    {
        $this->roles->clear();
    }

}
