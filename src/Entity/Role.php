<?php

namespace Duna\Security\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="plugin_role", indexes={@ORM\Index(name="parent_id", columns={"parent_id"})})
 * @ORM\Entity
 *
 * @method setName(string $name)
 * @method string getName()
 * @method setParent(Role $parent)
 * @method Role getParent
 */
class Role implements \Nette\Security\IRole
{

    const GUEST = 'guest';
    const USER = 'user';
    const ADMIN = 'admin';
    const SUPERADMIN = 'superadmin';

    use \Kdyby\Doctrine\Entities\MagicAccessors;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, nullable=false)
     */
    protected $name;
    /**
     * @var Role
     *
     * @ORM\OneToOne(targetEntity="Role", cascade={"persist"}, orphanRemoval=FALSE)
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="role_id", onDelete="cascade")
     */
    protected $parent;
    /**
     * @var integer
     *
     * @ORM\Column(name="role_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    public function getId()
    {
        return $this->id;
    }

    final public function getRoleId()
    {
        return $this->getId();
    }

}
