<?php

namespace Duna\Security;

use Kdyby\Doctrine\EntityManager;
use Nette;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Security
 */
class Authorizator implements Nette\Security\IAuthorizator
{


    const CREATE = 'create';
    const READ = 'read';
    const UPDATE = 'update';
    const DELETE = 'delete';
    const CACHE_NAMESPACE = 'System.Security';
    /** @var EntityManager */
    private $em;
    /** @var Nette\Caching\Cache */
    private $cache;
    /** @var Nette\Security\Permission */
    private $permission;

    public function __construct(EntityManager $em, Nette\Caching\IStorage $cache)
    {
        $this->em = $em;
        $this->cache = new Nette\Caching\Cache($cache, self::CACHE_NAMESPACE);
        $this->permission = new Nette\Security\Permission;

        if (PHP_SAPI === 'cli')
            return;

        $roles = $this->cache->load('roles', function (&$dep) {
            $dep = [Nette\Caching\Cache::TAGS => [self::CACHE_NAMESPACE . '/roles']];
            return $this->em->getRepository(Entity\Role::class)->findAll();
        });

        foreach ($roles as $role) {
            $this->permission->addRole($role->getName(), $role->getParent() ? $role->getParent()->getName() : null);
        }

        $resources = $this->cache->load('resources', function (&$dep) {
            $dep = [Nette\Caching\Cache::TAGS => [self::CACHE_NAMESPACE . '/resources']];
            return $this->em->getRepository(Entity\Resource::class)->findAll();
        });

        foreach ($resources as $resource) {
            $this->permission->addResource($resource->getResource());
        }

        $permissions = $this->cache->load('permissions', function (&$dep) {
            $dep = [Nette\Caching\Cache::TAGS => [self::CACHE_NAMESPACE . '/permissions']];
            $qb = $this->em->getRepository(Entity\Permission::class)->createQueryBuilder('perm');
            return $qb->join('perm.resource', 'reso')->addSelect('reso')
                ->join('perm.role', 'role')->addSelect('role')
                ->getQuery()->getResult();
        });

        foreach ($permissions as $permission) {
            if ($permission->create)
                $this->permission->allow($permission->role->name, $permission->resource->resource, self::CREATE);
            else
                $this->permission->deny($permission->role->name, $permission->resource->resource, self::CREATE);
            if ($permission->read)
                $this->permission->allow($permission->role->name, $permission->resource->resource, self::READ);
            else
                $this->permission->deny($permission->role->name, $permission->resource->resource, self::READ);

            if ($permission->update)
                $this->permission->allow($permission->role->name, $permission->resource->resource, self::UPDATE);
            else
                $this->permission->deny($permission->role->name, $permission->resource->resource, self::UPDATE);
            if ($permission->delete)
                $this->permission->allow($permission->role->name, $permission->resource->resource, self::DELETE);
            else
                $this->permission->deny($permission->role->name, $permission->resource->resource, self::DELETE);
        }
        $this->permission->allow(Entity\Role::SUPERADMIN, \Nette\Security\Permission::ALL, \Nette\Security\Permission::ALL);
    }

    public function isAllowed($role, $resource, $privilege)
    {
        try {
            return $this->permission->isAllowed($role, $resource, $privilege);
        } catch (Nette\InvalidStateException $ex) {
            return false;
        }
    }

}
