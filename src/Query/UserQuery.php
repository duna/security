<?php

namespace Duna\Security\Query;

use Kdyby\Persistence\Queryable;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Security\Query
 */
class UserQuery extends \Kdyby\Doctrine\QueryObject
{

    private $filter = [];
    private $select = [];

    protected function doCreateCountQuery(Queryable $repository)
    {
        $qb = $this->createBasicDql($repository)
            ->select('COUNT(user.id) as total_count');
        return $qb;
    }

    protected function doCreateQuery(Queryable $repository)
    {
        $qb = $this->createBasicDql($repository);
        foreach ($this->select as $modifier) {
            $modifier($qb);
        }
        $qb->addOrderBy('user.email', 'DESC');
        return $qb;
    }

    public function createBasicDql(Queryable $repository)
    {
        $qb = $repository->createQueryBuilder('user', 'user.id');
        foreach ($this->filter as $modifier) {
            $modifier($qb);
        }
        return $qb;
    }

}
