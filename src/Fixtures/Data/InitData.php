<?php

namespace Duna\Security\Fixtures\Init;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Duna\Console\IInsertData;
use Duna\Security\Facade\RoleFacade;
use Duna\Security\Facade\UserFacade;

class InitData implements IInsertData
{

    private $defaultRoles = [
        [
            'parent' => null,
            'name'   => 'superadmin',
        ],
        [
            'parent' => 'superadmin',
            'name'   => 'admin',
        ],
        [
            'parent' => 'admin',
            'name'   => 'user',
        ],
        [
            'parent' => 'user',
            'name'   => 'guest',
        ],
    ];
    private $defaultUsers = [
        [
            'name'     => 'Admin',
            'surname'  => 'Super',
            'email'    => 'superadmin@web.com',
            'roles'    => ['superadmin'],
            'password' => 'heslo',
        ],
        [
            'name'     => '',
            'surname'  => 'Admin',
            'email'    => 'admin@web.com',
            'roles'    => ['admin'],
            'password' => 'heslo',
        ],
        [
            'name'     => '',
            'surname'  => 'User',
            'email'    => 'user@web.com',
            'roles'    => ['user'],
            'password' => 'heslo',
        ],
    ];

    public function insert(EntityManagerInterface $em)
    {
        $facadeRole = new RoleFacade($em);
        foreach ($this->defaultRoles as $value) {
            $temp = $facadeRole->getOneByName($value['name']);
            if ($temp === null)
                $facadeRole->insert($value['name'], $value['parent']);
            else
                $facadeRole->update($temp->id, $value['name'], $value['parent']);
        }

        $facadeUser = new UserFacade($em);
        foreach ($this->defaultUsers as $user) {
            $temp = $facadeUser->getOneByEmail($user['email']);
            if ($temp === null)
                $facadeUser->insert($user['name'], $user['email'], $user['password'], $user['roles'], $user['surname']);
            else
                $facadeUser->update($temp->getId(), $user);
        }
    }
}