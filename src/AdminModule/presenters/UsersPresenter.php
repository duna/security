<?php

namespace Duna\Security\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use App\Components\Flash\Flash;
use Duna\Security\Components\IUserFormFactory;
use Duna\Security\Components\IUsersGrid;
use Duna\Security\Entity\User;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Security\AdminModule\Presenters
 */
class UsersPresenter extends BasePresenter
{

    private $editUser = null;

    public function actionEdit($id)
    {
        if ($id === null)
            $this->redirect('new');

        $this->editUser = $this->em->getRepository(User::class)->find($id);
        if ($this->editUser === null) {
            $this->flashMessage('Uživatel neexistuje.', Flash::DANGER);
            $this->redirect('new');

        }
    }

    public function renderEdit()
    {
        $template = $this->template;
        $template->userData = $this->editUser;
    }

    protected function createComponentUserForm($name, IUserFormFactory $factory)
    {
        $control = $factory->create($this, $name, $this->editUser);
        $that = $this;
        $control->onCreate[] = function () use ($that) {
            $that->flashMessage('Uživatel byl úspěšně vytvořen.', Flash::SUCCESS);
            $that->redirect('default');
        };
        $control->onUpdate[] = function () use ($that) {
            $that->flashMessage('Uživatel byl úspěšně upraven.', Flash::SUCCESS);
            $that->redirect('default');
        };

        return $control;
    }

    protected function createComponentUsersGrid(IUsersGrid $factory)
    {
        return $factory->create();
    }

}
