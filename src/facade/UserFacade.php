<?php

namespace Duna\Security\Facade;


use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Duna\Security\Entity\Role;
use Duna\Security\Entity\User;
use Nette\InvalidStateException;

class UserFacade
{
    /** @var  \Doctrine\ORM\EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param $id
     * @param bool $throwException
     * @return null|\Duna\Security\Entity\User
     */
    public function getById($id, $throwException = false)
    {
        $entity = $this->em->getRepository(User::class)->find($id);

        if ($entity === null && $throwException)
            throw new InvalidStateException();

        return $entity;
    }

    /**
     * @param $name
     * @param bool $throwException
     * @return null|\Duna\Security\Entity\User
     */
    public function getOneByEmail($email, $throwException = false)
    {
        $entity = $this->em->getRepository(User::class)->findOneBy([
            'email' => $email,
        ]);

        if ($entity === null && $throwException)
            throw new InvalidStateException();

        return $entity;
    }

    public function getPairs()
    {
        $qb = $this->em->createQueryBuilder()
            ->from(User::class, 'user');
        $query = $qb->select("user.surname", "user.name", "user.id")
            ->orderBy("user.surname, user.name")
            ->getQuery();

        $result = [];
        foreach ($query->getResult(AbstractQuery::HYDRATE_ARRAY) as $row) {
            $result[$row['id']] = "{$row['name']} {$row['surname']}";
        }
        return $result;
    }

    public function getReference($id)
    {
        return $this->em->getPartialReference(User::class, $id);
    }

    public function insert($name, $email, $password, array $roles, $surname = null, $throwException = false)
    {
        $entity = new User();
        $entity->setName($name);
        if ($surname)
            $entity->surname = $surname;
        $entity->email = $email;
        $entity->setPassword($password);
        foreach ($roles as $role) {
            if ($role instanceof Role) {
                $entity->addRole($role);
            } elseif (is_numeric($role)) {
                $facade = new RoleFacade($this->em);
                $entity->addRole($facade->getReference($role));
            } else {
                $facade = new RoleFacade($this->em);
                $entity->addRole($facade->getOneByName($role));
            }
        }
        $this->em->persist($entity);
        try {
            $this->em->flush($entity);
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw new InvalidStateException();
        }
    }

    /**
     * @param $id
     * @param array $data [
     *                    name => string,
     *                    surname => string,
     *                    email => string,
     *                    roles => mixed[]
     *                    ]
     * @param bool $throwException
     */
    public function update($id, array $data, $throwException = false)
    {
        try {
            $entity = $this->getById($id, true);
            if (array_key_exists('name', $data))
                $entity->setName($data['name']);
            if (array_key_exists('surname', $data))
                $entity->surname = $data['surname'];
            if (array_key_exists('email', $data))
                $entity->email = $data['email'];
            if (array_key_exists('password', $data))
                $entity->setPassword($data['password']);
            if (array_key_exists('roles', $data)) {
                $entity->clearRoles();
                foreach ($data['roles'] as $role) {
                    if ($role instanceof Role) {
                        $entity->addRole($role);
                    } elseif (is_numeric($role)) {
                        $facade = new RoleFacade($this->em);
                        $entity->addRole($facade->getReference($role));
                    } else {
                        $facade = new RoleFacade($this->em);
                        $entity->addRole($facade->getOneByName($role));
                    }
                }
            }
            $this->em->persist($entity);
            $this->em->flush($entity);
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw new InvalidStateException();
        } catch (InvalidStateException $e) {
            if ($throwException)
                throw new InvalidStateException();
        }
    }
}