<?php

namespace Duna\Security\Facade;


use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Duna\Security\Authorizator;
use Duna\Security\Entity\Resource;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\InvalidStateException;

class ResourceFacade
{
    /** @var \Doctrine\ORM\EntityManagerInterface */
    private $em;
    /** @var \Nette\Caching\Cache */
    private $cache = null;

    public function __construct(EntityManagerInterface $em, IStorage $storage = null)
    {
        $this->em = $em;
        if ($storage !== null)
            $this->cache = new Cache($storage, Authorizator::CACHE_NAMESPACE);
    }

    public function deleteByResource($resource)
    {
        $entity = $this->getByResource($resource, false);
        if ($entity !== null) {
            $this->em->remove($entity);
            $this->em->flush($entity);
        }
        $this->invalidateCache();
        return $entity;
    }

    public function getByResource($resource, $throwException = false)
    {
        $entity = $this->em->getRepository(Resource::class)->findOneBy([
            'resource' => $resource,
        ]);

        if ($entity === null && $throwException)
            throw new InvalidStateException();

        return $entity;
    }

    public function invalidateCache()
    {
        if ($this->cache !== null)
            $this->cache->clean([Cache::TAGS => [Authorizator::CACHE_NAMESPACE . '/resources']]);
    }

    public function deleteById($id)
    {
        $entity = $this->getById($id, false);
        if ($entity !== null) {
            $this->em->remove($entity);
            $this->em->flush($entity);
        }
        $this->invalidateCache();
        return $entity;
    }

    public function getById($id, $throwException = false)
    {
        $entity = $this->em->getRepository(Resource::class)->find($id);

        if ($entity === null && $throwException)
            throw new InvalidStateException();

        return $entity;
    }

    public function insert($name, $resource, $throwException = false)
    {
        $entity = new Resource();
        $entity->setName($name);
        $entity->setResource($resource);
        $this->em->persist($entity);

        try {
            $this->em->flush($entity);
            $this->invalidateCache();
            return $entity;
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw new InvalidStateException();
            return null;
        }
    }
}