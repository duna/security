<?php

namespace Duna\Security\Facade;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Duna\Security\Authorizator;
use Duna\Security\Entity\Permission;
use Duna\Security\Entity\Resource;
use Duna\Security\Entity\Role;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\InvalidStateException;

class PermissionFacade
{
    /** @var \Doctrine\ORM\Mapping\Entity */
    private $em;
    /** @var \Nette\Caching\Cache */
    private $cache;

    public function __construct(EntityManagerInterface $em, IStorage $storage = null)
    {
        $this->em = $em;
        if ($storage !== null)
            $this->cache = new Cache($storage, Authorizator::CACHE_NAMESPACE);
    }

    public function insert($create, $read, $update, $delete, $role, $resource, $throwException = false)
    {
        $entity = new Permission();
        $entity->setCreate($create)
            ->setRead($read)
            ->setUpdate($update)
            ->setDelete($delete);
        try {
            if ($role instanceof Role) {
                $entity->role = $role;
            } elseif (is_numeric($role)) {
                $facade = new RoleFacade($this->em);
                $entity->role = $facade->getById($role, true);
            } else {
                $facade = new RoleFacade($this->em);
                $entity->role = $facade->getOneByName($role, true);
            }

            if ($resource instanceof Resource) {
                $entity->resource = $resource;
            } elseif (is_numeric($resource)) {
                $facade = new ResourceFacade($this->em);
                $entity->resource = $facade->getById($resource, true);
            } else {
                $facade = new ResourceFacade($this->em);
                $entity->resource = $facade->getByResource($resource, true);
            }
            $this->em->persist($entity);
            $this->em->flush($entity);
            $this->invalidateCache();
            return $entity;
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw new InvalidStateException();
        }
    }

    public function invalidateCache()
    {
        if ($this->cache !== null)
            $this->cache->clean([Cache::TAGS => [Authorizator::CACHE_NAMESPACE . '/permissions']]);
    }

    public function deleteByRoleResource($role, $resource)
    {
        $entity = $this->getBy($role, $resource, false);
        if ($entity !== null) {
            $this->em->remove($entity);
            $this->em->flush($entity);
            $this->invalidateCache();
        }
        return $entity;
    }

    public function getBy($role, $resource, $throwException = false)
    {
        try {
            $entityRole = null;
            $entityResource = null;
            if ($role instanceof Role) {
                $entityRole = $role;
            } elseif (is_numeric($role)) {
                $facade = new RoleFacade($this->em);
                $entityRole = $facade->getById($role, true);
            } else {
                $facade = new RoleFacade($this->em);
                $entityRole = $facade->getOneByName($role, true);
            }
            if ($resource instanceof Resource) {
                $entityResource = $resource;
            } elseif (is_numeric($resource)) {
                $facade = new ResourceFacade($this->em);
                $entityResource = $facade->getById($resource, true);
            } else {
                $facade = new ResourceFacade($this->em);
                $entityResource = $facade->getByResource($resource, true);
            }

            $qb = $this->em->createQueryBuilder();
            $entity = $qb->select('perm')
                ->from(Permission::class, 'perm')
                ->join('perm.role', 'role')
                ->join('perm.resource', 'reso')
                ->where($qb->expr()->eq('role.id', $entityRole->getId()))
                ->andWhere($qb->expr()->eq('reso.id', $entityResource->id))
                ->getQuery()
                ->getOneOrNullResult();

            if ($entity === null && $throwException)
                throw new InvalidStateException();

            return $entity;
        } catch (InvalidStateException $e) {
            if ($throwException)
                throw new InvalidStateException();

            return null;
        }
    }
}