<?php

namespace Duna\Security\Facade;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Duna\Security\Entity\Role;
use Nette\InvalidStateException;

class RoleFacade
{
    /** @var  \Doctrine\ORM\EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param $id
     * @param bool $throwException
     * @return null|\Duna\Security\Entity\Role
     */
    public function getById($id, $throwException = false)
    {
        $entity = $this->em->getRepository(Role::class)->find($id);

        if ($entity === null && $throwException)
            throw new InvalidStateException();

        return $entity;
    }

    /**
     * @param $name
     * @param bool $throwException
     * @return null|\Duna\Security\Entity\Role
     */
    public function getOneByName($name, $throwException = false)
    {
        $entity = $this->em->getRepository(Role::class)->findOneBy([
            'name' => $name,
        ]);

        if ($entity === null && $throwException)
            throw new InvalidStateException();

        return $entity;
    }

    public function getReference($id)
    {
        return $this->em->getPartialReference(Role::class, $id);
    }

    public function insert($name, $parent, $throwException = false)
    {
        $entity = new Role();
        $entity->setName($name);
        if ($parent === null) {

        } elseif ($parent instanceof Role) {
            $entity->setParent($parent);
        } elseif (is_numeric($parent)) {
            $entity->setParent($this->getById((int) $parent, true));
        } else {
            $entity->setParent($this->getOneByName($parent, true));
        }
        $this->em->persist($entity);
        try {
            $this->em->flush($entity);
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw new InvalidStateException();
        }
    }

    /**
     * @param $id
     * @param array $data [
     *                    name => string,
     *                    parent => mixed
     *                    ]
     * @param bool $throwException
     */
    public function update($id, array $data, $throwException = false)
    {
        try {
            $entity = $this->getById($id, true);
            if (array_key_exists('name', $data))
            $entity->setName($data['name']);
            if (array_key_exists('parent', $data))
                $parent = $data['parent'];
            else $parent = null;
            if ($parent === null) {
                $entity->setParent(null);
            } elseif ($parent instanceof Role) {
                $entity->setParent($parent);
            } elseif (is_numeric($parent)) {
                $entity->setParent($this->getById((int) $parent, true));
            } else {
                $entity->setParent($this->getOneByName($parent, true));
            }
            $this->em->persist($entity);
            $this->em->flush($entity);
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw new InvalidStateException();
        } catch (InvalidStateException $e) {
            if ($throwException)
                throw new InvalidStateException();
        }
    }

    public function getAll(){
        return $this->em->getRepository(Role::class)->findAll();
    }
}