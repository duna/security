<?php

namespace Duna\Security;

use Kdyby\Doctrine\EntityManager;
use Nette;
use Nette\Security\Passwords;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Security
 */
class Authenticator implements Nette\Security\IAuthenticator
{

    /** @var EntityManager */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function authenticate(array $credentials)
    {
        $userName = $credentials[self::USERNAME];
        $password = $credentials[self::PASSWORD];

        $user = $this->em->getRepository(Entity\User::class)->findOneBy(['email eq' => $userName]);

        if (!$user) {
            throw new Nette\Security\AuthenticationException('Uživatelské jméno není správné.', self::IDENTITY_NOT_FOUND);
        } elseif (!Passwords::verify($password, $user->password)) {
            throw new Nette\Security\AuthenticationException('Zadané heslo není správné.', self::INVALID_CREDENTIAL);
        } elseif (Passwords::needsRehash($user->password)) {
            $user->password = Passwords::hash($password);
            $this->em->persist($user);
            $this->em->flush($user);
        }

        $roles = [];
        foreach ($user->roles as $role) {
            $roles[] = $role->name;
        }

        return new Nette\Security\Identity($user->getId(), $roles, Nette\Utils\ArrayHash::from([
            'name'    => $user->name,
            'surname' => $user->surname,
            'email'   => $user->email,
        ]));
    }

}
