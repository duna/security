<?php

namespace Duna\Security\Cli\Commands;

use Doctrine\ORM\Tools\ToolsException;
use Duna\Console\Command;
use Duna\Plugin\Manager\Facade;
use Duna\Security\DI\Extension;
use Duna\Security\Fixtures\Init\InitData;

class InstallCommand extends Command
{
    public function getDependentCommands()
    {
        return [
            ['command' => 'plugin:manager:install'],
        ];
    }

    public function getExtension()
    {
        return Extension::class;
    }

    public function getEntityMappings($onlyKey = false, $data = [])
    {
        return parent::getEntityMappings($onlyKey, Extension::entityMappings());
    }

    public function getData()
    {
        return [
            InitData::class
        ];
    }

    public function getTitle()
    {
        $entity = Extension::getPluginInfo()['plugin'];
        return $entity->description;
    }

    public function runCommand()
    {
        $facade = new Facade($this->em);
        $result = $this->getMetadataTables();
        try {
            $this->addMessageInfo('Creating database schema...');
            $this->schemaTool->updateSchema($result, true);
            $entity = Extension::getPluginInfo()['plugin'];
            if (!$facade->getOneByHash(md5($entity->name)))
                $facade->insert($entity->name, $entity->description);
            return 0;
        } catch (ToolsException $e) {
            return 1;
        }
    }

    protected function configure()
    {
        $this->setName('plugin:security:install')
            ->setDescription('Create database schema for security plugin.');
    }
}